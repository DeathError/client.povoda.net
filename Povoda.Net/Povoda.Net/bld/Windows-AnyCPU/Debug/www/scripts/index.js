﻿// Основные сведения о пустом шаблоне см. в следующей документации:
// http://go.microsoft.com/fwlink/?LinkID=397704
// Для отладки кода при загрузке страницы в Ripple, а также на устройства или в эмуляторы Android запустите приложение, задайте точки останова, 
// , а затем запустите "window.location.reload()" в консоли JavaScript.
var oauth,
    mySlidebars = new $.slidebars(),
	$output = $('#contents'),
    $page = $('#sb-site'),
    tokken = {
        access: 'null',
        resutl: false
    }
    page = {
        list: '5',
        type: 'home',
        selection: 'event'
    };
    app = {
        color_header: '#6d6d6d',
        icon_header: 'ic0.png',
        url_api: 'http://povoda.net/api/method/',
		status_network: false,
        initialize: function () {
            this.bindEvents();
            page.list = $page.data('list');
            page.type = $page.data('type');
            page.selection = $page.data('selection');
        },
        bindEvents: function () {// При старте
            app.getToken();
            //document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        onLoadsContent: function (_this, e, _load) {
            e.preventDefault();
            var $link = $(_this),
                requestMethod = $link.data('method'),
                responseType = $link.data('type'),
                name_method = $link.data('get'),
                url = app.url_api + '' + name_method + '?access_token=' + tokken.access + '&'+$link.attr('href');
            if (_load){
                $output.html('');
                $("#LoadingTemplate").tmpl([{ messages: 'Загрузка' }]).innerHTML("#contents");
            }
            $.ajax({
                url: url,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: requestMethod,
                dataType: responseType,
                success: function (data) {
                    mySlidebars.slidebars.close();
					app.ReColorStatusBar($link.data('cat'));
                    app.setStatusLoad({
                        _select: 'event',
                        _list: '5',
                        _type: $link.data('cat')
                    });
					if ('message' in data) {
						if ('user' in data.message) {
							app.ShowUserPage(data.message.user);
						}
						if ('event' in data.message) {
							app.ShowListPage(data.message.event);
						}
						if ('place' in data.message) {
							app.ShowListPage(data.message.place);
						}
						if ('search' in data.message) {
							app.ShowSearchPage(data.message.search);
						}
						if ('item' in data.message) {
							app.ShowItemtPage(data.message.item);
						}
						if ('error' in data) {
							$output.html(data.error_description);
						}
					}
                },
                error: function (jqXHR, textStatus) {
                    $output.html(textStatus);
                }
            });
        },
        setStatusLoad: function(param){
            page.selection = param._select;
            page.list = param._list;
            page.type = param._type;
            $page.attr('data-type',param._type);
            $page.attr('data-list',param._list);
            $page.attr('data-selection',param._select);
        },
        ReColorStatusBar: function(type){
            var color,
                icon;
            switch (type){
                case 'kino-teatri':         color = "#DB402A"; icon = 'ic1.png'; break;
                case 'klubi-kontserti':     color = "#2A1E74"; icon = 'ic2.png';  break;
                case 'eda-keytering':       color = "#35763D"; icon = 'ic3.png';  break;
                case 'moda-shoping':        color = "#E6782D"; icon = 'ic4.png';  break;
                case 'deti-i-vnuki':        color = "#838280"; icon = 'ic5.png';  break;
                case 'otdih-turizm':        color = "#95B232"; icon = 'ic6.png';  break;
                case 'krasota-zdorove':     color = "#CE9629"; icon = 'ic7.png';  break;
                case 'sport-sorevnovaniya': color = "#8F5444"; icon = 'ic8.png';  break;
                case 'vistavki-prezentatsii':color = "#6E609E"; icon = 'ic9.png';  break;
                case 'seminari-treningi':   color = "#3896BC"; icon = 'ic10.png';  break;
                default: color = "#6d6d6d"; icon = 'ic0.png';
            }
            app.color_header = color;
            $('.navbar-fixed-top').css({
                backgroundColor: color
            }).attr('data-color',type);
            $('.navbar-fixed-top .icon_product').css({
                backgroundImage: 'img'+icon
            });
            StatusBar.backgroundColorByHexString(color);
        },
        ShowHomePage: function (param) {
            var url = app.url_api + 'getlist' + '?access_token=' + tokken.access + '&'+param;
            $.ajax({
                url: url,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    mySlidebars.slidebars.close();
                    $output.html('');
                  
                    $("#listTemplate").tmpl(data.message.event).appendTo("#contents");
                    app.ReColorStatusBar('home');
                },
                error: function (jqXHR, textStatus) {
                    $output.html(textStatus);
                }
            });
        },
        ShowUserPage: function (user) {
            $( "#clientTemplate" ).tmpl(user).appendTo( "#contents" );
        },
		ShowSearchPage: function (event) {
            $( "itemTemplate" ).tmpl(event).appendTo( "#contents" );
        },
        ShowListPage: function (event) {
            $("#listTemplate").tmpl(event).appendTo("#contents");
        },
        ShowItemtPage: function (event) {
            $( "#itemTemplate" ).tmpl(event).appendTo( "#contents" );
        },
        LoadScrolItem: function(ob){
            var SQL,
                url;
            if (page.type!='home'){
                SQL = 'param_cat='+page.type+'&param_selection='+page.selection+'&param_filter[limit]='+(page.list+1)+'|3';
            } else {
                SQL = 'param_selection='+page.selection+'&param_filter[limit]='+(page.list+1)+'|3';
            }
            app.setStatusLoad({
                _select: page.selection,
                _list: page.list+5,
                _type: page.type
            });

            url = app.url_api + 'getlist' + '?access_token=' + tokken.access + '&'+SQL;
            $.ajax({
                url: url,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    if ("place" in data.message) {
                        $("#listTemplate").tmpl(data.message.place).appendTo("#contents");
                    } else {
                        $("#listTemplate").tmpl(data.message.event).appendTo("#contents");
                    }
                },
                error: function (jqXHR, textStatus) {
                    $output.html(textStatus);
                }
            });
        },
        getToken: function(){
            var param = '?client_id=client_povoda.net&client_secret=daniilddr000&grant_type=client_credentials';
            $.ajax({
                url: app.url_api + 'authorize'+param,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: 'POST',
                dataType: "json",
                success: function (data) {
                    tokken.access = data.access_token;
                    tokken.resutl = true;
                    if (page.type == 'home') {
                        app.ShowHomePage('param_selection=event&param_filter[limit]=1|5"');
                    }
                },
                error: function (jqXHR, textStatus) {
                    $output.html(textStatus);
                }
            });
        }
    };

(function () {
    "use strict";
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    $(document).delegate('a.cors', 'click', function (e) {
		if (app.status_network){
			app.onLoadsContent(this, e, true);
		} else {
		    app.onLoadsContent(this, e, true);
		    $("#ErrorTemplate").tmpl([{ messages: 'Нет подключения к сити' }]).appendTo( "#contents" );
		}
	});

    $page.swipe( {
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            if (direction == 'up'){
                app.LoadScrolItem();
            }
        }
    });

    $(document).scroll(function() {
        var $this = $("#looadFotoPage");
        if (($(window).scrollTop() + $(window).height() == $(document).height()) && ($($this).data('end') != 'end')) {
            app.LoadScrolItem();
        }
    });

    function onDeviceReady() {
        // Обработка событий приостановки и возобновления Cordova
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
		document.addEventListener("offline", onOffline, false);
		document.addEventListener("online", onOnline, false);
        // TODO: Платформа Cordova загружена. Выполните здесь инициализацию, которая требуется Cordova.
    };

	function onOnline() {
		//https://github.com/apache/cordova-plugin-network-information/blob/master/doc/ru/index.md
		// TODO: Это событие возникает, когда приложение выходит в онлайн, и устройство подключается к Интернету.
	}
	
	function onOffline() {
		app.status_network = false;
		// TODO: Событие возникает, когда приложение переходит в автономный режим, и устройство не подключено к сети Интернет.
	}

    function onPause() {
		app.status_network = true;
        // TODO: Это приложение приостановлено. Сохраните здесь состояние приложения.
    };

    function onResume() {
        app.getToken();
        // TODO: Это приложение активировано повторно. Восстановите здесь состояние приложения.
    };

})();


app.initialize();