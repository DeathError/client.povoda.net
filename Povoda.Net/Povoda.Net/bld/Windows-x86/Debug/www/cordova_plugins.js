﻿cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-network-information/www/network.js",
        "id": "cordova-plugin-network-information.network",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/cordova-plugin-network-information/www/Connection.js",
        "id": "cordova-plugin-network-information.Connection",
        "clobbers": [
            "Connection"
        ]
    },
    {
        "file": "plugins/cordova-plugin-network-information/src/windows/NetworkInfoProxy.js",
        "id": "cordova-plugin-network-information.NetworkInfoProxy",
        "merges": [
            ""
        ]
    },
    {
        "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
        "id": "cordova-plugin-statusbar.statusbar",
        "clobbers": [
            "window.StatusBar"
        ]
    },
    {
        "file": "plugins/cordova-plugin-statusbar/src/windows/StatusBarProxy.js",
        "id": "cordova-plugin-statusbar.StatusBarProxy",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-websql/www/WebSQL.js",
        "id": "cordova-plugin-websql.WebSQL",
        "merges": [
            "window"
        ]
    },
    {
        "file": "plugins/cordova-plugin-websql/www/windows/Database.js",
        "id": "cordova-plugin-websql.Database"
    },
    {
        "file": "plugins/cordova-plugin-websql/www/windows/SqlTransaction.js",
        "id": "cordova-plugin-websql.SqlTransaction"
    },
    {
        "file": "plugins/cordova-plugin-websql/src/windows/WebSqlProxy.js",
        "id": "cordova-plugin-websql.WebSqlProxy",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-dialogs/www/notification.js",
        "id": "cordova-plugin-dialogs.notification",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/cordova-plugin-dialogs/src/windows/NotificationProxy.js",
        "id": "cordova-plugin-dialogs.NotificationProxy",
        "merges": [
            ""
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-network-information": "1.0.1",
    "cordova-plugin-statusbar": "1.0.1",
    "cordova-plugin-websql": "0.0.10",
    "cordova-plugin-whitelist": "1.0.0",
    "cordova-plugin-dialogs": "1.1.1"
}
// BOTTOM OF METADATA
});