﻿// Основные сведения о пустом шаблоне см. в следующей документации:
// http://go.microsoft.com/fwlink/?LinkID=397704
// Для отладки кода при загрузке страницы в Ripple, а также на устройства или в эмуляторы Android запустите приложение, задайте точки останова, 
// , а затем запустите "window.location.reload()" в консоли JavaScript.


/**
 * Основной для скрипт - Едро приложения
 */

(function () {
    "use strict";
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    /**
     * Нажатие на пункты меню
     */
    $(document).delegate('a.cors', 'click', function (e) {
        app.isStartConnect();
        page.this_temp = null;
        page.item = false;
        activity.lists.setMaxItem();
		if (app.status_network){
			app.onLoadsContent(this, e, true);
		} else {
		    app.errorConect(app.param.textNoConect);
		}
		e.preventDefault();
    });

    /**
     * Открываем фильтр
     */
    $(document).delegate('.sb-toggle-search', 'click', function (e) {
        e.preventDefault();
        navigator.notification.vibrate(100);
        app.openPanelFilter(true);
    });

    /**
    * Нажатие при обновлении подключения
    */
    $(document).delegate('#PagesReflase', 'click', function (e) {
        app.isStartConnect();
        navigator.notification.vibrate(100);
        page.this_temp = null;
        if (app.status_network) {
            app.getToken();
        } else {
            app.errorConect(app.param.textNoConect);
        }
    });

    /**
     * Расшарить в соц сетях
     */
    $(document).delegate('.sb-toggle-sharing', 'click', function (e) {
        e.preventDefault();
        navigator.notification.vibrate(100);
        var description = page.this_temp.description,
            messages =
                activity.htmlSpeshar(description) + '\r\n\r\n'+
                'Дата начала: '+ page.this_temp.dateShow + '\r\n'+
                'Время начала: '+ page.this_temp.time + '\r\n'+
                'Место: '+ page.this_temp.address + '\r\n'+
                'Телефон: '+ page.this_temp.phone1 + '\r\n';
        window.plugins.socialsharing.share(
            messages,
            page.this_temp.title,
            'http://povoda.net/content/comps/'+page.this_temp.id+'/big_'+page.this_temp.img,
            'http://povoda.net/item/'+page.this_temp.id
        )
        cordova.plugins.notification.local.schedule({
            title: page.this_temp.title,
            message:
            'Дата начала: '+ page.this_temp.dateShow + '\r\n'+
            'Время начала: '+ page.this_temp.time + '\r\n'+
            'Место: '+ page.this_temp.address + '\r\n'+
            'Телефон: '+ page.this_temp.phone1 + '\r\n',
            sound: "file://sounds/message.mp3",
            icon: 'http://povoda.net/content/comps/'+page.this_temp.id+'/big_'+page.this_temp.img
        });
    });
    /**
     * выход из события
     */
    $(document).delegate('.sb-toggle-back', 'click', function (e) {
        e.preventDefault();
        navigator.notification.vibrate(100);
        var SQL = 'param_cat=' + page.type + '&param_selection=' + page.selection + '&param_filter[limit]=0|' + page.list;
        app.ReColorStatusBar(page.type);
        activity.setNewHeight();
        page.this_temp = null;
        app.ShowBacList(SQL, page.type);
    });

    /**
     * Поиск
     */
    $(document).delegate('#form_search button', 'click', function (e) {
        e.preventDefault();
        navigator.notification.vibrate(100);
        activity.filter.readParam(activity.filter.objects);
        app.getFilter(activity.filter);
    });

    /**
     * Работа пальцами на страницы
     */
 
    $header.swipe({
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
            switch (direction) {
                case 'up': app.LoadScrolItem(); break;
                case 'left': app.swipeHeaderReflase(false); break;
                case 'right': app.swipeHeaderReflase(true); break;
            }
        }
    });

    /**
     * Работа мышкой или клавиатурой
     */
    $(document).scroll(function() {
        var $this = $("#looadFotoPage");
        activity.lists.scrollItem();

        if (($(window).scrollTop() + $(window).height() + 100 >= $(document).height()) && ($($this).data('end') != 'end')) {
            app.LoadScrolItem();
        }
        //if (($(window).scrollTop() - $(window).height() == $(document).height())) {
        //    console.log($(window).scrollTop() - $(window).height() +'=='+ $(document).height())
        //    app.LoadScrolItem();
        //}
        if (!activity.header.didScroll) {
            activity.header.didScroll = true;
            setTimeout(activity.scrollPage(), 230);
        }
       
    });

    /**
     * Обработка событий приостановки и возобновления Cordova
     */
    function onDeviceReady() {
        //var dbSize = 5 * 1024 * 1024; // 5MB
        //
        //var db = openDatabase("settings", "", "Settings app Povoda.Net", dbSize, function () {
        //    console.log('db successfully opened or created');
        //});
        //
        //db.transaction(function (tx) {
        //    tx.executeSql("CREATE TABLE IF NOT EXISTS settings(ID INTEGER PRIMARY KEY ASC, param TEXT, option TEXT)", [], onSuccess, onError);
        //    tx.executeSql('INSERT INTO settings(param, option) VALUES ("push", "true")', [], onSuccess, onError);
        //});

        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        document.addEventListener("offline", onOffline, false);
        document.addEventListener("online", onOnline, false);
        document.addEventListener("backbutton", onBackKeyDown, false);
        document.addEventListener("menubutton", onMenuKeyDown, false);

        watchID = navigator.geolocation.getCurrentPosition(onSuccessGeo, onError);
        // TODO: Платформа Cordova загружена. Выполните здесь инициализацию, которая требуется Cordova.
    };

    /**
    * Обработчка ошибок подключения
    */
    function onError(transaction, error) {
        //console.log('Query failed: ' + error.message);
    }

    /**
   * Координаты пользователя
   */
    function onSuccessGeo(pos) {
        var crd = pos.coords;
        app.coords = crd;
        console.log('Your current position is:');
        console.log('Latitude : ' + crd.latitude);
        console.log('Longitude: ' + crd.longitude);
        console.log('More or less ' + crd.accuracy + ' meters.');
    };


    /**
    * Мы в БД
    */
    function onSuccess(transaction, resultSet) {
        console.log(transaction);
        console.log(resultSet);
        app.param.enablePush = resultSet.push;
        console.log('Query completed: ' + JSON.stringify(resultSet));
    }

    /**
    * Вызов меню на кномку
    */
    function onMenuKeyDown() {
        if (activity.menu.didScroll && !mySlidebars.slidebars.active('left')){
            mySlidebars.slidebars.open('left');
        }
        // TODO: Handle the back button
    }

    /**
    * Обработка кнопки назад
    */
    function onBackKeyDown() {
        var SQL, exits = true, if_close_item = false;
        if (activity.filter.status){ // результат поиска
            SQL = activity.filter.getSQL((page.list + 1)+'|10');
            page.type = activity.filter.cat_id;
            page.list = page.list + 1;
            exits = false;
            if_close_item = false
        } else {
            if (page.type!='home'){
                activity.header.setShowItem(false);
                exits = false;
                if ($('#contents .block-list-items .block-item').length >= 1) { // сли есть список
                    SQL = 'param_selection=' + page.selection + '&param_filter[sort]=list&param_filter[limit]=0|10';
                    page.list = 10;
                    page.type = 'home';
                    if_close_item = false
                } else {
                    if_close_item = true;
                    SQL = 'param_cat=' + page.type + '&param_selection=' + page.selection + '&param_filter[limit]=0|' + page.list;
                    page.list = page.list + 1;
                }
            }
        }
        if(mySlidebars.slidebars.active('right')){
            mySlidebars.slidebars.close();
            exits = false;
        } else {
            app.setStatusLoad({
                _select: page.selection,
                _list: page.list,
                _type: page.type
            });
            app.ReColorStatusBar(page.type);
            activity.setNewHeight();
            if (if_close_item) {
                page.this_temp = null;
                app.ShowBacList(SQL, page.type);
            }
            if (!if_close_item && !exits) {
                app.ShowHomePage(SQL, page.type);
            }
        }
        page.this_temp = null;
        page.item = false;
        if (exits) {
            exitAppPopup();
        }
        // TODO: Handle the back button
    }

    /**
    * Сеть включена на телефоне
    */
    function onOnline() {
        app.status_network = true;
		//https://github.com/apache/cordova-plugin-network-information/blob/master/doc/ru/index.md
		// TODO: Это событие возникает, когда приложение выходит в онлайн, и устройство подключается к Интернету.
	}

    /**
    * Отключения интернета
    */
    function onOffline() {
		app.status_network = false;
		// TODO: Событие возникает, когда приложение переходит в автономный режим, и устройство не подключено к сети Интернет.
	}

    /**
    * Приложени свёрнуто
    */
	function onPause() {
        app.isStartConnect();
        //cordova.plugins.notification.local.schedule({
        //    text: "У нас есть событие специально для ва!",
        //    sound: "file://sounds/alert.caf",
        //    every: 30 // every 30 minutes
        //});
        // TODO: Это приложение приостановлено. Сохраните здесь состояние приложения.
    };

    /**
    * Заново активно
    */
    function onResume() {
        app.getToken(onResume);
        app.isStartConnect();
        // TODO: Это приложение активировано повторно. Восстановите здесь состояние приложения.
    };

    /**
    * Хочет выйти?
    */
    function exitAppPopup() {
        navigator.notification.confirm(
            "Вы и в правду хотите выйти :-(",
            function(buttonIndex){
                ConfirmExit(buttonIndex);
            },
            "Закрыть?",
            "Да,Нет"
        );
        //return false;
    };

    /**
    * Выхлд из приложения
    */
    function ConfirmExit(stat){
        if(stat == "1"){
            navigator.app.exitApp();
        } else {
            app.isStartConnect();
            page.this_temp = null;
            page.type = 'home';
            app.getToken();
        };
    };
})();

app.initialize();
