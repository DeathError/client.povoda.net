﻿// Специальные переопределения платформы будут помещены в версии папки слияний этого файла

var docElem = document.documentElement,
    $sbRight = $('.sb-right'),
    activity = {
        /**
         * height = Тикущий размер
         * height_min = Минимальный разворот шапки
         * changeHeaderOn = через сколько пикселей вклюючить анимацию
         * didScroll = Работает ли анимация
         * is_mini = Свёрнуто или развёрнута шапка
         */
        header: {
            height: 151,
            height_min: 49,
            changeHeaderOn: 50,
            didScroll: false,
            is_mini: false,
            /**
             * Функция для определения какие кнопки показывать
             * @param pageItem - истона или лож. Если истина то кнопки для страницы
             */
            setShowItem: function(pageItem){
                if(pageItem){
                    $('.sb-toggle-back').removeClass('hidden');
                    $('.sb-toggle-sharing').removeClass('hidden');
                    $('.sb-toggle-left').addClass('hidden');
                    $('.sb-toggle-search').addClass('hidden');
                } else {
                    $('.sb-toggle-left').removeClass('hidden');
                    $('.sb-toggle-search').removeClass('hidden');
                    $('.sb-toggle-back').addClass('hidden');
                    $('.sb-toggle-sharing').addClass('hidden');
                }
            }
        },
        lists: {
            minShow: 5,
            maxShow: 10,
            thisItem: 0,
            objects: $('#LoadItemContent .block-item'),
            setParam: function(param){
                activity.lists.minShow = param.minShow;
                activity.lists.maxShow = param.maxShow;
            },
            visibleItem: function (_this) {
                var $this = $(_this);
                $this.removeClass('el-hidden');

            },
            removeItem: function (_this) {
                var $this = $(_this);
                setTimeout(function(){
                   // $(window).scrollTop($(window).scrollTop()-$this.outerHeight());
                    $this.addClass('el-hidden');
                },350);
            },
            setMaxItem: function(){
                if ( $('.block-item').length >= 1){
                    $('.block-item').each(function (_index, _label) {
                        var $this = $(_label);
                        if ($this.is(":in-viewport")){
                            $this.addClass('visible');
                        }
                    });
                }
                setTimeout(function(){
                    activity.lists.maxShow = $('.block-item.visible').length;
                },300)
            },
            scrollItem: function () {
                var $object = $('.block-item');
                if ($object.length >= 1) {
                    activity.lists.thisItem = $object.length;
                    $object.each(function (_index, _label) {
                        var $this = $(_label),
                            scroll = $(window).scrollTop(),
                            ptop = $this.offset().top;
                        if (scroll-ptop >= $this.outerHeight()) {
                            activity.lists.removeItem($this)
                        } else {
                            activity.lists.visibleItem($this);
                        }
                    });
                }
            }
        },
        menu: {
            didScroll: true,
            getNumberMenu: function(_name){
                var $this = $('.list_item_menu a[data-cat='+_name+']'),
                    number = 0;
                if ($this.length >=1){
                    number = $this.data('number');
                }
                return number;
            }
        },
        filter: {
            status: false,
            search: '',
            cat_id: '',
            area: '',
            date: '',
            date2: '',
            is_vip: 0,
            ic_vip: 0,

            objects: $('#form_search'),
            setParam: function(param){
                activity.filter.search = param.search;
                activity.filter.cat_id = param.cat_id;
                activity.filter.area = param.area;
                activity.filter.date = param.date;
                activity.filter.date2 = param.date2;
                activity.filter.is_vip = param.is_vip;
                activity.filter.ic_vip = param.ic_vip;
            },
            getSQL: function(_limit){
                var param_cat_id, SQL;
                if (activity.filter.cat_id != 'home') {
                    param_cat_id = 'param_cat=' + activity.filter.cat_id + '';
                }
                SQL = param_cat_id +
                '&param_selection=' + page.selection + '' +
                '&param_filter[limit]=' + _limit +
                '&param_filter[date]=' + activity.filter.date +
                '&param_filter[date2]=' + activity.filter.date2 +
                '&param_filter[is_vip]=' + activity.filter.is_vip +
                '&param_filter[ic_vip]=' + activity.filter.ic_vip +
                '&param_filter[area]=' + activity.filter.area +
                '&param_filter[search]=' + activity.filter.search
                return SQL;
            },
            showSelect: function(){
                var $this = $(activity.filter.objects),
                    myselect = $('select[name="cat_id"]',$this);
                myselect[0].selectedIndex = activity.menu.getNumberMenu(page.type);
                myselect.selectmenu("refresh");
            },
            readParam: function(_this){
              var $this = $(_this),
                  hData = {};
                $('input, textarea, select',  $this).each(function(i, e){
                    var $el = $(e);
                    if($el[0].name && $el[0].name!=''){
                        if ($el[0].type == 'checkbox' || $el[0].type == 'radio'){
                            if($el.attr('checked')){
                                hData[$el[0].name] = $el[0].value;
                            } else {
                                hData[$el[0].name] = 0;
                            }
                        } else {
                            hData[$el[0].name] = $el[0].value;
                        }
                    }
                });
                this.setParam(hData);
            }
        },
        /**
         * ВПредзагрузка
         */
        preloader: {
            status: false,
            objects: $('.des-preloader'),
            load: function(times){
                activity.preloader.on();
                if (times<0 | times == 'undefend'){
                    times = 0;
                }
                window.onload = function() {
                    activity.preloader.off();
                };
                setTimeout(function(){
                    activity.preloader.off();
                },times);
            },
            off: function(){
                activity.preloader.objects.addClass('hidden');
                activity.preloader.status = false;
            },
            on: function(){
                activity.preloader.objects.addClass('showPage').removeClass('hidden');
                activity.preloader.status = true;
            },
            hidden: function () {
                setTimeout(function () {
                    activity.preloader.objects.toggleClass('hidden');
                }, 100);
                if (activity.preloader.objects.hasClass('hidden')) {
                    activity.preloader.status = false;
                } else {
                    activity.preloader.status = true;
                }
                
            }
        },
        /**
         * Действие при скроле
         */
        scrollPage: function () {
            var sy = activity.scrollY();
            if (sy >= activity.header.changeHeaderOn) {
                activity.header.is_mini = true;
                activity.setNewHeight();
            } else {
                activity.header.is_mini = false;
                activity.setNewHeight();
            }
            activity.header.didScroll = false;
        },
        /**
         * Установливаем размер шапки
         */
        setNewHeight: function(){
            $('.sb-init').toggleClass('mini-header');
            if (activity.header.is_mini){
                $header.addClass('cbp-af-header-shrink').css({'height':  activity.heightHeader()+ 'px'});
            } else {
                $header.removeClass('cbp-af-header-shrink').css({'height': activity.heightHeader() + 'px'});
            }
            $page.css({'padding-top': activity.heightHeader() + 'px'});
            $sbRight.css({'margin-top': activity.heightHeader() + 'px'});
        },
        scrollY: function() {
            return window.pageYOffset || docElem.scrollTop;
        },
        heightHeader: function(){
            return $('.wr.head_wr', $header).outerHeight(true);
        },
        showParamFilter: function(){
            if (page.type=='home'){
                activity.filter.cat_id = '';
            } else {
                activity.filter.cat_id = page.type;
            }
        },
        configCarouFredSel: function(){
            $('#slider').carouFredSel({
                    width: '100%',
                    mousewheel: true,
                    swipe: true,
                    align: 'auto',
                    items: {
                        width: '100%',
                        visible: {
                            min: 1,
                            max: 1
                        }
                    },
                    scroll: {
                        items: 1,
                        easing: "cubic", //Indicates which easing function to use for the transition. jQuery defaults: "linear" and "swing", built in: "quadratic", "cubic" and "elastic".
                        // duration: 1000,
                        pauseOnHover: true,
                        onBefore: function (data) {
                            var objects = data.items.old;
                            unhighlight(objects);
                        },
                        onAfter: function (data) {
                            var objects = data.items.visible;
                            highlight(objects);
                        }
                    },
                    pagination: {
                        container: "#slider_pagination",
                        easing: scroll.easing,
                        onBefore: scroll.onBefore,
                        onAfter: scroll.onAfter
                    },
                    auto: false
                }, {
                    debug: false,
                    transition: true,
                    wrapper: {
                        element: "div",
                        classname: "caroufredsel_slider"
                    }
                }
            ).mousedown(function (eventObject) {
                    activity.menu.didScroll = false;
                }).mouseup(function (eventObject) {
                    activity.menu.didScroll = true;
                });
            function highlight(items) {
                items.addClass("active");
            }
            function unhighlight(items) {
                items.removeClass("active");
            }
            $(document).resize(function() {
                activity.configCarouFredSel();
            });
        },
        YandexMapsInit: function (item, flags) {
            var vacancies = [],
                map;
            if (flags == 1) {
                var contGroup = Object.keys(item).length;
                for (var i = 0; i < contGroup; i++) {
                    var indexs = 'item_' + i;
                    vacancies[i] = {
                        "id": item[indexs].id,
                        "title": item[indexs].title+' '+ item[indexs].date + ' в ' + item[indexs].time,
                        "description": '<b>' + item[indexs].address + '</b><br/>' + activity.htmlSpecialChars(item[indexs].description, true),
                        "address": "г. Воронеж, " + item[indexs].address
                    }
                }
                map = new ymaps.Map('map', {
                    center: [51.68, 39.15],
                    zoom: 12.6,
                    behaviors: ['drag', 'scrollZoom'],
                    controls: []
                });
            } else {
                vacancies = [{
                    "id": item.id,
                    "title": item.address,
                    "address": "г. Воронеж, " + item.address
                }
                ];
                map = new ymaps.Map('map', {
                    center: [51.68, 39.15],
                    zoom: 12.6,
                    behaviors: ['dblClickZoom'], //, 'scrollZoom'
                    controls: []
                });
            }
            var ZoomLayout = ymaps.templateLayoutFactory.createClass(
                    "<div class='panel_maps_navigation'>" +
                    "<div id='zoom-in' class='btn zoom-in'><i class='icon-plus'></i></div><br>" +
                    "<div id='zoom-out' class='btn zoom-out'><i class='icon-minus'></i></div>" +
                    "</div>",
                    {
                        build: function () {
                            // Вызываем родительский метод build.
                            ZoomLayout.superclass.build.call(this);
                            this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                            this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);
                            $('#zoom-in').bind('click', this.zoomInCallback);
                            $('#zoom-out').bind('click', this.zoomOutCallback);
                        },

                        clear: function () {
                            $('#zoom-in').unbind('click', this.zoomInCallback);
                            $('#zoom-out').unbind('click', this.zoomOutCallback);
                            ZoomLayout.superclass.clear.call(this);
                        },
                        zoomIn: function () {
                            var myMap = this.getData().control.getMap();
                            this.events.fire('zoomchange', {
                                oldZoom: myMap.getZoom(),
                                newZoom: myMap.getZoom() + 1
                            });
                        },
                        zoomOut: function () {
                            var myMap = this.getData().control.getMap();
                            this.events.fire('zoomchange', {
                                oldZoom: myMap.getZoom(),
                                newZoom: myMap.getZoom() - 1
                            });
                        }
                    }),
                zoomControl = new ymaps.control.ZoomControl({
                    options: {
                        layout: ZoomLayout,
                        position: {
                            right: '5px',
                            left: 'auto',
                            top: '15px'
                        }
                    }});

            map.controls.add(zoomControl);
            clusterer = new ymaps.Clusterer({
                groupByCoordinates: false,
                clusterDisableClickZoom: false,
                clusterBalloonContentBodyLayout: "cluster#balloonAccordionContent"
            });
            clusterer.events.once('objectsaddtomap', function () {
                map.setBounds(clusterer.getBounds() /*myMap.geoObjects.getBounds()*/, {
                    checkZoomRange: true
                });
            });
            map.geoObjects.add(clusterer);
            collection = [];
            if (vacancies.length) {
                for (var i in vacancies) {
                    var myGeocoder = ymaps.geocode(""+vacancies[i].address+"");
                    myGeocoder.then(
                        (function (i) {
                        return function (res) {
                            coords = res.geoObjects.get(0).geometry.getCoordinates();
                            if (flags == 1) {
                                pl = new ymaps.Placemark(coords, {
                                    balloonContentHeader: vacancies[i].title,
                                    balloonContent: vacancies[i].description,
                                    hintContent: vacancies[i].address,
                                    extId: vacancies[i].id
                                });
                            } else {
                                pl = new ymaps.Placemark(coords, {
                                    balloonContentBody: vacancies[i].address,
                                    hintContent: vacancies[i].address,
                                    extId: vacancies[i].id
                                });
                            }
                            collection.push(pl);
                            if (collection.length == vacancies.length) {
                                clusterer.add(collection);
                                map.setCenter(coords, 12);
                            }
                        }
                    })(i));
                }
            }
        },
        htmlSpecialChars: function(string, reverse) {
            var specialChars = {
                    "&": "&amp;",
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;"
                }, x;
            if (typeof(reverse) != "undefined") {
                reverse = [];
                for (x in specialChars)
                    reverse.push(x);
                reverse.reverse();
                for (x = 0; x < reverse.length; x++)
                    string = string.replace(
                        new RegExp(specialChars[reverse[x]], "g"),
                        reverse[x]
                    );
                return string;
            }
            for (x in specialChars)
                string = string.replace(new RegExp(x, "g"), specialChars[x]);
            return string;
        },
        htmlSpeshar: function(string, reverse){
            var specialChars = {
                "&amp;": "&",
                "&lt;": "<",
                "&gt;": ">",
                '&quot;': '"',
                '&laquo;': '"',
                '&raquo;': '"',
                '&ndash;': '\r\n'
            }, x;
            if (typeof(reverse) != "undefined") {
                reverse = [];
                for (x in specialChars)
                    reverse.push(x);
                reverse.reverse();
                for (x = 0; x < reverse.length; x++)
                    string = string.replace(
                        new RegExp(specialChars[reverse[x]], "g"),
                        reverse[x]
                    );
                return string;
            }
            for (x in specialChars)
                string = string.replace(new RegExp(x, "g"), specialChars[x]);
            return string;
        }
    };