/**
 * Created by NAU on 06.10.2015.
 */

var oauth,
    watchID,
    mySlidebars = new $.slidebars(),
    $output = $('#contents'),
    $header = $('#header'),
    $page = $('#sb-site'),
    $titles = $('#titlePages'),
    tokken = {
        access: 'null',
        resutl: false
    },
    user_coords = {
        string: '[51.68, 39.15]'
    }
    page = {
        list: 5,
        type: 'home',
        selection: 'event',
        this_temp: '9f68b345c86686bbe7a7446a2df96fd1b67f21d7',
        item: false
    },
    app = {
        coords: {
            latitude: '',
            longitude: ''
        },
        param:{
            openItem: 0,
            enablePush: false,
            textNoConect: 'Нет подключения к сети',
            errorText: function(textStatus){
                var $text = textStatus,
                    textResult;
                switch ($text){
                    case 0: textResult= 'Нет данных'; break;
                    case 'error': textResult= 'Неверный формат запроса'; break;
                    default:  textResult= 'Нет подключения к сети';
                }
                return textResult
            }
        },
        color_header: '#6d6d6d',
        icon_header: 'ic0.png',
        url_api: 'http://povoda.net/api/method/',
        status_network: false,
        /**
         * Иницилизируем функции
         */
        initialize: function () {
            app.isStartConnect();
            this.bindEvents();
            page.list = parseInt($page.data('list'));
            page.type = $page.data('type');
            page.selection = $page.data('selection');
        },
        /**
         * При старте
         */
        bindEvents: function () {
            if (app.status_network){
                app.getToken();
            } else {
                app.errorConect(app.param.textNoConect);
            }
        },
        /**
         * Развернули откно заново
         * @param ob
         * @constructor
         */
        restartPage: function(){
            activity.filter.showSelect();
            activity.header.setShowItem(false);
        },
        /**
         * Открываем меню
         * @param _this = что за элемнет
         * @param e = данные элемента
         * @param _load = грузить заново или подгружать
         */
        onLoadsContent: function (_this, e, _load) {
            var $link = $(_this),
                requestMethod = $link.data('method'),
                responseType = $link.data('type'),
                name_method = $link.data('get'),
                url = app.url_api + '' + name_method + '?access_token=' + tokken.access + '&'+$link.attr('href');
            if (_load){
                $output.html('');
                $("#LoadingTemplate").tmpl([{ messages: 'Загрузка' }]).appendTo("#contents");
            }
            if (name_method == 'getmaps') {
                url = url + '&param_filter[geo]=' + user_coords.string;
            }
            activity.preloader.on();
            activity.header.setShowItem(false);
            activity.filter.status = false;
            console.log(url);
            $.ajax({
                url: url,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: requestMethod,
                dataType: responseType,
                success: function (data) {
                    mySlidebars.slidebars.close();
                    app.ReColorStatusBar($link.data('cat'));
                    page_list_is = page.list;
                    $output.html('');
                    activity.filter.showSelect();
                    if ('message' in data) {
                        if ('banner' in data.message) {
                            app.ShowBanner(data.message.banner);
                        }
                        if ('user' in data.message) {
                            app.ShowUserPage(data.message.user);
                        }
                        if ('maps' in data.message) {
                            app.ShowUserPage(data.message.maps);
                        }
                        if ('event' in data.message) {
                            page_list_is = 5;
                            app.ShowListPage(data.message.event);
                        }
                        if ('place' in data.message) {
                            page_list_is = 5;
                            app.ShowListPage(data.message.place);
                        }
                        if ('search' in data.message) {
                            page_list_is = 5;
                            app.ShowSearchPage(data.message.search);
                        }
                        if ('item' in data.message) {
                            app.ShowItemtPage(data.message.item);
                        }
                        if ('error' in data) {
                            app.errorConect(app.param.textNoConect);
                        }
                    }
                    app.setStatusLoad({
                        _select: 'event',
                        _list: page_list_is,
                        _type: $link.data('cat')
                    });
                },
                error: function (jqXHR, textStatus) {
                    app.errorConect(app.param.errorText(textStatus));
                }
            });
            return false;
        },
        /**
         * Где мы сейчас
         * @param param - параметры для запросов
         */
        setStatusLoad: function(param){
            page.selection = param._select;
            page.list = param._list;
            page.type = param._type;
            $page.attr('data-type',param._type);
            $page.attr('data-list',param._list);
            $page.attr('data-selection',param._select);
        },
        /**
         * Меняем шапку на цвет, заголовок
         * @param type - Тип страницы который пришёл
         * @constructor
         */
        ReColorStatusBar: function(type){
            var color,
                icon,
                titles = $('.list_item_menu a[data-cat="'+type+'"]').html();
            $('.none_mini').remove();
            switch (type){
                case 'kino-teatri':         color = "#DB402A"; icon = 'ic1.png'; break;
                case 'klubi-kontserti':     color = "#2A1E74"; icon = 'ic2.png';  break;
                case 'eda-keytering':       color = "#35763D"; icon = 'ic3.png';  break;
                case 'moda-shoping':        color = "#E6782D"; icon = 'ic4.png';  break;
                case 'deti-i-vnuki':        color = "#838280"; icon = 'ic5.png';  break;
                case 'otdih-turizm':        color = "#95B232"; icon = 'ic6.png';  break;
                case 'krasota-zdorove':     color = "#CE9629"; icon = 'ic7.png';  break;
                case 'sport-sorevnovaniya': color = "#8F5444"; icon = 'ic8.png';  break;
                case 'vistavki-prezentatsii':color = "#6E609E"; icon = 'ic9.png';  break;
                case 'seminari-treningi':   color = "#3896BC"; icon = 'ic10.png';  break;
                case 'home': color = "#6d6d6d"; icon = 'ic0.png'; titles = 'Povoda.Net'; $('<span class="none_mini">городской портал</span>').appendTo("#header .block_title_page"); break;
                case 'maps': color = "#6d6d6d"; icon = 'ic0.png'; titles = 'Povoda.Net'; $('<span class="none_mini">городской портал</span>').appendTo("#header .block_title_page"); break;
                default: {
                    color = "#6d6d6d";
                    icon = 'ic0.png';
                    titles = 'Povoda.Net';
                }
            }
            $titles.html(titles);
            app.color_header = color;
            $('.navbar-fixed-top').css({
                backgroundColor: color
            }).attr('data-color',type);
            $('.sb-slidebar.sb-right').css({
                backgroundColor: color
            });
            StatusBar.backgroundColorByHexString(color);
        },
        /**
         * Открыть как главную страницу
         * @param param - Пазаметры запуска
         * @param page - какая страница
         * @constructor
         */
        ShowBanner: function (banner) {
            $("#bannerTemplate").tmpl(banner).appendTo("#contents");
        },
        /**
         * Вернутся из карточки товара в раздел
         * @param param - Пазаметры запуска
         * @param page_type - какая страница
         * @constructor
         */
        ShowBacList:  function (param , page_type) {
            activity.filter.status = false;
            var url = app.url_api + 'getlist' + '?access_token=' + tokken.access + '&' + param,
                page_item = page.item;
            activity.preloader.on();
            console.log(url);
            $.ajax({
                url: url,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    mySlidebars.slidebars.close();
                    activity.header.setShowItem(false);
                    $output.html('');
                    if ('event' in data.message) {
                        $("#listTemplate").tmpl(data.message.event).appendTo("#contents");
                        activity.preloader.hidden();
                        app.ReColorStatusBar(page_type);
                        activity.lists.setMaxItem();
                        if ($('#item_' + page_item).length > 0) {
                            setTimeout(function () {
                                $(window).scrollTop($('#item_' + page_item).offset().top);
                            }, 30);
                        }
                    } else {
                        app.errorConect(app.param.textNoConect);
                    }
                },
                error: function (jqXHR, textStatus) {
                    app.errorConect(app.param.errorText(textStatus));
                }
            });
        },
        /**
         * Открыть главную страницу
         * @param param - Пазаметры запуска
         * @param page - какая страница
         * @constructor
         */
        ShowHomePage: function (param , page) {
            activity.filter.status = false;
            var url = app.url_api + 'getlist' + '?access_token=' + tokken.access + '&' + param + '&param_filter[banner]=is_home';
            activity.preloader.on();
            $.ajax({
                url: url,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    mySlidebars.slidebars.close();
                    activity.header.setShowItem(false);
                    $output.html('');
                    if ('banner' in data.message) {
                        app.ShowBanner(data.message.banner);
                    }
                    if ('event' in data.message) {
                        $("#listTemplate").tmpl(data.message.event).appendTo("#contents");
                        app.ReColorStatusBar(page);
                        activity.lists.setMaxItem();
                    } else {
                        app.errorConect(app.param.textNoConect);
                    }
                    activity.preloader.off();
                },
                error: function (jqXHR, textStatus) {
                    app.errorConect(app.param.errorText(textStatus));
                }
            });
        },
        /**
         * Данные пользователя / Будем использовать для карты
         * @param param - Пазаметры запуска
         * @param page - какая страница
         * @constructor
         */
        ShowUserPage: function (maps) {
            $("#clientTemplate").tmpl(maps).appendTo("#contents");
            activity.preloader.off();
            ymaps.ready(activity.YandexMapsInit(maps,1));
        },
        /**
         * Вывод результат поиска
         * @param event - данные
         * @constructor
         */
        ShowSearchPage: function (event) {
            activity.header.is_mini = true;
            activity.setNewHeight();
            activity.header.didScroll = true;
            $("itemTemplate").tmpl(event).appendTo("#contents");
            page.this_temp = event;
            page.item = true;
            activity.preloader.hidden();
        },
        /**
         * Вывод списка
         * @param event - данные
         * @constructor
         */
        ShowListPage: function (event) {
            activity.header.is_mini = false;
            activity.header.didScroll = false;
            activity.setNewHeight();
            app.param.openItem = event.id;
            if (app.status_network) {
                $("#listTemplate").tmpl(event).appendTo("#contents");
            } else {
                app.errorConect(app.param.errorText(0));
            }
            activity.preloader.hidden();

        },
        /**
         * Вывод карточки
         * @param event - данные
         * @constructor
         */
        ShowItemtPage: function (event) {
            if ('description' in event){
                event.description = activity.htmlSpecialChars(event.description, true);
            }
            page.item = event.id;
            page.type = event.category.article;
            activity.header.is_mini = true;
            activity.header.didScroll = true;
            activity.setNewHeight();
            activity.header.setShowItem(true);
            $("#itemTemplate").tmpl(event).appendTo("#contents");
            activity.preloader.hidden();
            page.this_temp = event;
            activity.configCarouFredSel();
            ymaps.ready(activity.YandexMapsInit(event,0));
        },
        /**
         * Подгрузка контента
         * @param ob
         * @constructor
         */
        LoadScrolItem: function (ob) {
            var SQL,
                url,
                plus = 5,
                page_list = '0|5';
            if (ob==1){
                page_list = (page.list-plus)+'|'+plus;
            }
            if (activity.filter.status == true){
                page.type = activity.filter.cat_id;
                SQL = activity.filter.getSQL((page.list)+'|'+plus);
            } else {
                plus = 3;
                if (page.type!='home'){
                    SQL = 'param_cat='+page.type+'&param_selection='+page.selection+'&param_filter[limit]='+(page.list)+'|'+plus;
                } else {
                    SQL = 'param_selection='+page.selection+'&param_filter[limit]='+(page.list)+'|'+plus;
                }
            }
            app.setStatusLoad({
                _select: page.selection,
                _list: page.list+plus,
                _type: page.type
            });
            if ($('#looadFotoPage').length >= 1) {
                url = app.url_api + 'getlist' + '?access_token=' + tokken.access + '&' + SQL;
                console.log(url);
                $.ajax({
                    url: url,
                    data: 'data sent to the server on ' + (new Date()).toString(),
                    contentType: 'text/plain',
                    type: 'GET',
                    dataType: "json",
                    success: function (data) {
                        var contents = [{'eror': 'error'}];
                        if ("place" in data.message) {
                            contents=data.message.place;
                        } else {
                            contents=data.message.event;
                        }
                        $("#itemTemplateLoad").tmpl(contents).appendTo("#LoadItemContent");
                    },
                    error: function (jqXHR, textStatus) {
                        app.errorConect(app.param.errorText(textStatus));
                    }
                });
            }
        },
        /**
         * Получения токена
         */
        getToken: function (onResume) {
            var param = '?client_id=client_povoda.net&client_secret=' + "ZGFuaWlsZGRyMDAw" + '&grant_type=client_credentials';
            if (onResume != 'onResume') {
                activity.preloader.on();
            }
            $.ajax({
                url: app.url_api + 'authorize'+param,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: 'POST',
                dataType: "json",
                success: function (data) {
                    tokken.access = data.access_token;
                    tokken.resutl = true;
                    if (page.type == 'home') {
                        app.ShowHomePage('param_selection=event&param_filter[sort]=list&param_filter[limit]=0|5"', page.type);
                    }
                },
                error: function (jqXHR, textStatus) {
                    app.errorConect(app.param.errorText(textStatus));
                }
            });
        },
        /**
         * Получения фильтра
         * @param ob
         * @constructor
         */
        getFilter: function(param){
            var SQL, type = 'home', param_cat_id ='';
            if (param.cat_id!=''){
                type = param.cat_id;
            }
            app.setStatusLoad({
                _select: page.selection,
                _list: 10,
                _type: type
            });
            mySlidebars.slidebars.close();
            activity.setNewHeight();
            SQL = activity.filter.getSQL('0|100');
            var url = app.url_api + 'getlist' + '?access_token=' + tokken.access + '&' + SQL;
            $.ajax({
                url: url,
                data: 'data sent to the server on ' + (new Date()).toString(),
                contentType: 'text/plain',
                type: 'GET',
                dataType: "json",
                success: function (data) {
                    mySlidebars.slidebars.close();
                    $output.html('');
                    if(data.message.event != ''){
                        activity.filter.status = true;
                        $("#listTemplate").tmpl(data.message.event).appendTo("#contents");
                        activity.preloader.off();
                    } else {
                        navigator.notification.confirm(
                            "Попробуем другой запрос?",
                            function(buttonIndex){
                                if(1){
                                    app.openPanelFilter(true);
                                } else {
                                    var param_cat_id = '';
                                    if (page.type != 'home') {
                                        param_cat_id = 'param_cat=' + page.type + '&';
                                    }
                                    app.ShowHomePage(param_cat_id+'param_selection=event&param_filter[limit]=0|5"', page.type);
                                }
                            },
                            "Фильтер",
                            "Да,Нет"
                        );
                    }
                    app.ReColorStatusBar(type);
                },
                error: function (jqXHR, textStatus) {
                    app.errorConect(app.param.errorText(textStatus));
                }
            });
        },
        /**
         * Если нет интернета
         */
        errorConect: function(text){
            $output.html('');
            mySlidebars.slidebars.close();
            $("#ErrorTemplate").tmpl([{ messages: text }]).appendTo( "#contents" );
            app.ReColorStatusBar('home');
        },
        /**
         * Проверяем есть ли интернет
         * @param ob
         * @constructor
         */
        isStartConnect: function () {
            app.status_network = true;
            if (navigator.connection.type=='none') {
                app.status_network = false;
            } else {
                app.status_network = true;
            }
        },
        /**
         * Для карточки - необходимо менять кнопки
         * @param ob
         * @constructor
         */
        swipeHeaderReflase: function (is_right) {
            var $this = $('.list_item_menu a[data-cat="' + page.type + '"]').parent(),
                $object;
            if (is_right) {
                $object = $($this).prev();
            } else {
                $object = $($this).next();
            }
            if ($object.length >= 1){
                app.onLoadsContent($('a',$object ),false, true);
            }
        },
        /**
         * Если фильтер открыт
         * @param ob
         * @constructor
         */
        openPanelFilter: function(_t){
            if (_t){
                mySlidebars.slidebars.toggle('right');
            } else {
                mySlidebars.slidebars.open('right');
            }
            app.ReColorStatusBar(page.type);
            activity.header.is_mini = true;
            activity.setNewHeight();
            setTimeout(function(){
                activity.header.didScroll = mySlidebars.slidebars.active('right');
            },400);
        }
    };